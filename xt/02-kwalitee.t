#!/usr/bin/perl -w

use Test::More;
use Test::Kwalitee qw[kwalitee_ok];

# strictness check ignores import of Moose in packages, which are strict
kwalitee_ok qw[
    -use_strict
];

done_testing;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
