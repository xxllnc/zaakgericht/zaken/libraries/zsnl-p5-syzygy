package Syzygy::Test::Object::ValueTypes::Date;

use Test::Class::Moose;
use Test::Fatal;

use DateTime;
use Syzygy::Object::ValueTypes::Date;
use Syzygy::Object::ValueTypes::String;

sub test_object_value_type_date {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::Date->new }
        'date value type instantiation lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::Date',
        'date value type constructor retval';

    is $type->name, 'date', 'date value type has expected name';

    my $now_dt = DateTime->now(time_zone => 'floating');
    my $yesterday_dt = DateTime->now(time_zone => 'floating')->subtract(days => 1);

    my ($now, $yesterday);

    lives_ok {
        $now = $type->new_value($now_dt)
    } 'date value type instantiates now';

    lives_ok {
        $yesterday = $type->new_value($yesterday_dt)
    } 'date value type instantiates yesterday';

    isa_ok $now, 'Syzygy::Object::Value', 'date value type value instantiator retval';
    isa_ok $yesterday, 'Syzygy::Object::Value', 'date value type value instantiator retval';

    ok $type->equal($now, $now), 'now == now: positive equality';
    ok $type->equal($yesterday, $yesterday), 'yesterday == yesterday: positive equality';

    ok !$type->equal($now, $yesterday), 'now == yesterday: negative equality';
    ok !$type->equal($yesterday, $now), 'yesterday == now: negative equality';

    is_deeply $type->deflate_value($now), $now_dt->ymd,
        'type value deflation returns Z-postfixed ISO8601 date string';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
