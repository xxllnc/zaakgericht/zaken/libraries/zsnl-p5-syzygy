package Syzygy::Test::Object::ValueTypes::String;

use Test::Class::Moose;
use Test::Fatal;

use Syzygy::Object::ValueTypes::String;
use Syzygy::Object::ValueTypes::Text;

sub test_object_value_type_text {
    my $type;

    lives_ok { $type = Syzygy::Object::ValueTypes::String->new }
        'string value type instantiation lives';

    isa_ok $type, 'Syzygy::Object::ValueTypes::String',
        'string value type constructor retval';

    is $type->name, 'string', 'string value type has expected name';

    my ($foo, $bar);

    lives_ok { $foo = $type->new_value('abc') } 'string value type instantiates abc value';
    lives_ok { $bar = $type->new_value('xyz') } 'string value type instantiates xyz value';

    isa_ok $foo, 'Syzygy::Object::Value', 'value constructor retval abc';
    isa_ok $bar, 'Syzygy::Object::Value', 'value constructor retval xyz';

    ok $type->equal($foo, $foo), 'abc == abc: positive equality';
    ok !$type->equal($foo, $bar), 'abc == xyz: negative equality';
    ok !$type->not_equal($foo, $foo), 'abc != abc: negative non-equality';
    ok $type->not_equal($foo, $bar), 'abc != xyz: positive non-equality';

    is_deeply $type->deflate_value($foo), 'abc',
        'type value deflation returns plain string';

    is_deeply $type->deflate_value($bar), 'xyz',
        'type value deflation returns another plain string';
}

sub test_object_value_type_text_coercions {
    my $type = Syzygy::Object::ValueTypes::String->new;

    my $string = Syzygy::Object::ValueTypes::Text->new->new_value('abc');

    my $coerced = $type->new_value($string);

    is $coerced->type_name, 'string', 'coerced value has expected type';

    is $coerced->value, $string->value, 'coerced value preserves content';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
