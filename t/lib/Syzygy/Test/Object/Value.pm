package Syzygy::Test::Object::Value;

use Test::Class::Moose;

use Syzygy::Object::Value;
use Syzygy::Object::ValueTypes::String;

sub test_object_value_interface {
    my $type = Syzygy::Object::ValueTypes::String->new;
    my $value;

    lives_ok {
        $value = Syzygy::Object::Value->new(
            type => $type
        );
    } 'object value instantiation lives';

    ok !$value->has_value, 'object value with no actual value has_value == false';
    is $value->type_name, $value->type->name, 'object value has expected type_name';

    dies_ok {
        $value->value('abc');
    } 'values are immutable when created without value';

    ok !defined $value->as_graph_value,
        'undefined object value has undefined graoh_value';

    $value = Syzygy::Object::Value->new(
        type => $type,
        value => 'abc'
    );

    ok $value->has_value, 'object value definedness predicate ok';

    dies_ok {
        $value->value('xyz');
    } 'values are immutable when created with value';

    is $value->value, 'abc', 'value unchanged after immutable update';

    is $value->value, $value->as_graph_value,
        'object value graph_value evaluation returns expected value'
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
