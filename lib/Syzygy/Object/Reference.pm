package Syzygy::Object::Reference;
our $VERSION = '0.006';
use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::ObjectReference
];

=head1 NAME

Syzygy::Object::Reference - Object reference base class

=head1 DESCRIPTION

=cut

use Syzygy::Types qw[ObjectType UUID];

# Convenience for reference construction, ignore preview key if present but
# value undefined. This makes sure we get the default preview string behavior
# as defined in Syzygy::Interface::ObjectReference.
around BUILDARGS => sub {
    my ($orig, $class, %params) = @_;

    if (exists $params{ preview }) {
        delete $params{ preview } unless defined $params{ preview };
    }

    return $class->$orig(%params);
};

=head1 ATTRIBUTES

=head2 id

String that uniquely identifies an object of the referenced L</type>.

=cut

# BEGIN wrapper for id requirement from ObjectReference role
BEGIN {
    has id => (
        is => 'ro',
        isa => UUID,
        required => 1
    );
}

=head2 preview

Human-readable preview string of the referenced object.

=cut

# BEGIN wrapper for preview requirement from ObjectReference role
BEGIN {
    has preview => (
        is => 'ro',
        isa => 'Str',
        lazy => 1, # Prevent catch-22 during instance construction
        default => sub { shift->_as_string }
    );
}

=head2 type

Reference to an object that implements L<Syzygy::Interface::ObjectType>.

=cut

# BEGIN wrapper for type_name requirement from ObjectReference role
BEGIN {
    has type => (
        is => 'ro',
        isa => ObjectType,
        required => 1,
        handles => {
            type_name => 'name'
        }
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
