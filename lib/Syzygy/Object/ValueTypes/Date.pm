package Syzygy::Object::ValueTypes::Date;
our $VERSION = '0.006';
use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::ValueTypes::Datetime';

=head1 NAME

Syzygy::Object::ValueTypes::Date - C<date> value behavior and validation

=head1 DESCRIPTION

This value type abstracts the notion of a date value.

=cut

use DateTime::Format::ISO8601;
use Syzygy::Types qw[Datestamp];

=head1 METHODS

=head2 name

Overrides L<Syzygy::Object::ValueTypes::Datetime/name> with a method that
returns the C<date> type name.

=cut

override name => sub { return 'date' };

=head2 perl_type_constraint

Overrides L<Sysygy::Object::ValueTypes::Datetime/perl_type_constraint> and
returns a date-specific variant of the same constraint.

=cut

override perl_type_constraint => sub { return Datestamp };

=head2 deflate_value

Overrides L<Syzygy::Object::ValueTypes::Datetime/deflate_value>.

=cut

override deflate_value => sub {
    my $self = shift;
    my $value = shift;

    return $value->value->ymd;
};

=head2 parse_datetime_string

Overrides L<Syzygy::Object::ValueTypes::Datetime/parse_datetime_string>.

=cut

override parse_datetime_string => sub {
    my $self = shift;
    my $value = shift;

    # Silence parser's kicking and screaming when invalid data is entered, we
    # check the value anyway.
    my $dt = eval { DateTime::Format::ISO8601->parse_datetime("$value") };

    if (defined $dt) {
        return $dt->truncate(to => 'day')->set_time_zone('floating');
    }

    throw('syzygy/object/value_type/date/parsing_error', sprintf(
        'Attempted to coerce "%s" as datetime, cannot parse or invalid datetime string',
        $value
    ));
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
