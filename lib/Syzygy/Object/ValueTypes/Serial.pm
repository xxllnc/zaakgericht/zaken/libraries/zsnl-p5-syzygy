package Syzygy::Object::ValueTypes::Serial;
our $VERSION = '0.006';
use Moose;
use namespace::autoclean;

with qw[
    Syzygy::Interface::ValueType
];

=head1 NAME

Syzygy::Object::ValueTypes::Serial - Value type for the
L<Syzygy::Types/SerialNumber> type.

=head1 DESCRIPTION

This value type abstracts the concept of a sequence/serial/autoincremental
number.

=cut

use Syzygy::Types qw[SerialNumber];

=head1 METHODS

=head2 name

Returns the static type name for this value type (C<serial>).

=cut

sub name {
    return 'serial';
}

=head2 perl_type_constraint

Implements L<Syzygy::Interface::ValueType/perl_type_constraint>.

=cut

sub perl_type_constraint {
    return SerialNumber;
}

=head2 equal

Implements equality testing of two values in C<serial> context.

=cut

sub equal {
    my $self = shift;
    my $a = $self->coerce(shift);
    my $b = $self->coerce(shift);

    return unless defined $a && defined $b;
    return unless $a->has_value && $b->has_value;

    return $a->value == $b->value;
}

=head2 not_equal

Implements inequality testing of two values in C<serial> context.

=cut

sub not_equal { return not shift->equal(@_) }

=head2 coerce

Implements L<Syzygy::Interface::ValueType/coerce>.

=cut

sub coerce {
    my $self = shift;
    my $value = shift;

    return unless $value->has_value;

    return unless $value->type_name eq 'number';

    return $self->new_value($value->value);
}

=head2 deflate_value

Implements L<Syzygy::Interface::ValueType/deflate_value>.

=cut

sub deflate_value {
    my $self = shift;
    my $value = shift;

    # Extra safe-guard against perl string->number automagic.
    return int($value->value);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Syzygy::CONTRIBUTORS> file.

Syzygy uses the EUPL license, for more information please have a look at the L<LICENSE|Syzygy::LICENSE> file.
